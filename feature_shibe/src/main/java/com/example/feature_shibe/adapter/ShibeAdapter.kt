package com.example.feature_shibe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.feature_shibe.databinding.ItemShibeBinding
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.model.local.entity.Shibe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(
    private val shibes: List<Shibe>, private val repo: ShibeRepo
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url, repo)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe, repo: ShibeRepo) {
            binding.imgItem.load(shibe.url)
            binding.favoriteImg.isVisible = shibe.isLiked
            binding.imgItem.setOnClickListener {
                shibe.isLiked = !shibe.isLiked
                CoroutineScope(Dispatchers.IO).launch { repo.shibeDao.update(shibe) }
                binding.favoriteImg.isVisible = shibe.isLiked
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}

package com.example.feature_shibe.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.feature_shibe.R
import com.example.feature_shibe.model.ShibeRepo
import com.example.feature_shibe.viewmodel.ShibeViewModel
import com.example.feature_shibe.adapter.ShibeAdapter
import com.example.feature_shibe.databinding.FragmentShibeListBinding

class ShibeListFragment : Fragment(R.layout.fragment_shibe_list) {

    private var _binding: FragmentShibeListBinding? = null
    private val binding get() = _binding!!
    private val repo by lazy { ShibeRepo(requireContext()) }
    private val shibeViewModel by viewModels<ShibeViewModel>() {
        ShibeViewModel.ShibeViewModelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.run {
                rvShibeList.apply {
                    progress.isVisible = state.isLoading
                    adapter = ShibeAdapter(state.shibes, repo)
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.example.feature_drink.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.feature_drink.model.BottomsUpRepo
import com.example.feature_drink.view.drinkdetial.DrinkDetailState
import kotlinx.coroutines.launch

class DrinkDetailViewModel : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _state = MutableLiveData(DrinkDetailState(isLoading = true))
    val state: LiveData<DrinkDetailState> get() = _state

    fun getDrinkDetails(drink: Int) {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinkDetails(drink)
            _state.value = DrinkDetailState(drinkDetails = drinkDetailsDTO)
        }
    }
}
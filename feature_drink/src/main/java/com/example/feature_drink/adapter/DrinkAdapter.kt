package com.example.feature_drink.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.feature_drink.databinding.ItemDrinksListBinding
import com.example.feature_drink.model.response.CategoryDrinksDTO
import com.squareup.picasso.Picasso

class DrinkAdapter(private val drinkDetail: (Int) -> Unit) :
    RecyclerView.Adapter<DrinkAdapter.DrinkViewHolder>() {

    private var category = mutableListOf<CategoryDrinksDTO.Drink>()

    /* Set up View Holder */
    // This method inflates card layout items for Recycler View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DrinkViewHolder.getInstance(parent)

    // This method sets the data to specific views of card items.
    // It also handles methods related to clicks on items of Recycler view.
    override fun onBindViewHolder(holder: DrinkViewHolder, position: Int) {
        val categoryData = category[position]
        holder.bindDrink(categoryData)
        holder.returnDrinkDetail().setOnClickListener {
            drinkDetail(categoryData.idDrink.toInt())
        }
    }

    // This method returns the length of the RecyclerView.
    override fun getItemCount(): Int {
        return category.size
    }

    // === Custom Function to pass object[it] ===
    // viewModel gets category and assigns data to category variable
    fun loadDrinks(item: List<CategoryDrinksDTO.Drink>) {
        this.category = item.toMutableList()
        notifyDataSetChanged()
    }

    class DrinkViewHolder(
        private val binding: ItemDrinksListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        // This function gets the data from category member variable / position and
        // passes data as an argument to this function
        fun bindDrink(category: CategoryDrinksDTO.Drink) {
//            binding.idDrink.text = category.idDrink
            binding.strDrink.text = category.strDrink
            Picasso.get().load(category.strDrinkThumb).into(binding.drinkThumb)
        }

        fun returnDrinkDetail(): ImageView {
            return binding.drinkThumb
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinksListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinkViewHolder(it) }
        }
    }
}
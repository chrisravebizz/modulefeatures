package com.example.feature_drink.view.drink

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.feature_drink.adapter.DrinkAdapter
import com.example.feature_drink.viewmodel.DrinkViewModel
import com.example.feature_drink.databinding.FragmentDrinkBinding

class DrinkFragment : Fragment() {

    private var _binding: FragmentDrinkBinding? = null
    private val binding get() = _binding!!
    private val drinkViewModel by viewModels<DrinkViewModel>()
    private val args by navArgs<DrinkFragmentArgs>()
    private val drinkAdapter by lazy { DrinkAdapter(::drinkClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkViewModel.getDrinksList(args.drink)
        drinkViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.loader.isVisible = state.isLoading
            binding.tvHeaderDrink.text = args.drink + "'s"
            binding.rvDrink.adapter = drinkAdapter.apply { loadDrinks(state.drinks) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun drinkClicked(drink: Int) {
        val drinkDetail = DrinkFragmentDirections.goToDrinkDetailFragment(drink)
        findNavController().navigate(drinkDetail)
    }
}
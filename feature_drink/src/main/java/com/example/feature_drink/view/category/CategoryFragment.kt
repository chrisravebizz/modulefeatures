package com.example.feature_drink.view.category

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.feature_drink.adapter.CategoryAdapter
import com.example.feature_drink.viewmodel.CategoryViewModel
import com.example.feature_drink.databinding.FragmentCategoryBinding

class CategoryFragment : Fragment() {

    private var _binding: FragmentCategoryBinding? = null
    private val binding get() = _binding!!
    private val categoryViewModel by viewModels<CategoryViewModel>()
    private val categoryAdapter by lazy { CategoryAdapter(::categoryClicked) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentCategoryBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoryViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.loader.isVisible = state.isLoading
            binding.rvText.adapter = categoryAdapter.apply { loadCategory(state.categories) }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun categoryClicked(drink: String) {
        val drinkList = CategoryFragmentDirections.goToDrinkFragment(drink)
        findNavController().navigate(drinkList)
    }
}
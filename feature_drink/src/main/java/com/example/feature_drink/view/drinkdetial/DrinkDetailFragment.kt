package com.example.feature_drink.view.drinkdetial

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.example.feature_drink.adapter.DrinkDetailAdapter
import com.example.feature_drink.viewmodel.DrinkDetailViewModel
import com.example.feature_drink.databinding.FragmentDrinkDetailBinding

class DrinkDetailFragment : Fragment() {

    private var _binding: FragmentDrinkDetailBinding? = null
    private val binding get() = _binding!!
    private val drinkDetailViewModel by viewModels<DrinkDetailViewModel>()
    private val args by navArgs<DrinkDetailFragmentArgs>()
    private val drinkDetailAdapter by lazy { DrinkDetailAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentDrinkDetailBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        drinkDetailViewModel.getDrinkDetails(args.drink)
        drinkDetailViewModel.state.observe(viewLifecycleOwner) { drink ->
            binding.loader.isVisible = drink.isLoading
            binding.rvName.adapter =
                drinkDetailAdapter.apply { loadDrinkDetail(drink.drinkDetails) }
        }
    }

}
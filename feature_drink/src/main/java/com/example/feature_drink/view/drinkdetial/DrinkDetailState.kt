package com.example.feature_drink.view.drinkdetial

import com.example.feature_drink.model.response.DrinkDetailsDTO

class DrinkDetailState(
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)
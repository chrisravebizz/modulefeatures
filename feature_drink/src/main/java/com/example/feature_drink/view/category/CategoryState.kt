package com.example.feature_drink.view.category

import com.example.feature_drink.model.response.CategoryDTO

class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<CategoryDTO.CategoryItem> = emptyList()
)
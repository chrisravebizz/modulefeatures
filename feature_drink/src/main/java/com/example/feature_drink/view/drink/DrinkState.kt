package com.example.feature_drink.view.drink

import com.example.feature_drink.model.response.CategoryDrinksDTO

class DrinkState(
    val isLoading: Boolean = false,
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)